#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "libro.h"

/* Questa funzione rimuove, da una stringa terminata dal carattere null,
    i caratteri "\n" e " " se stanno nella prima posizione o nell'ultima */
void PulisciStringa(char s[])
{
     // Controlliamo l'ultimo carattere
    if (s[strlen(s)-1] == '\n' || s[strlen(s)-1] == ' ')
    {
        s[strlen(s)-1] = '\0'; // accorciamo la stringa
        PulisciStringa(s); // e ricontrolliamo
    }

     // Controlliamo il primo carattere
    if (s[0] == '\n' || s[0] == ' ')
    {
        int i;
        for(i=0; i<strlen(s)-1; i++) s[i]=s[i+1]; // Spostiamo la stringa di un carattere carattere
        PulisciStringa(s); // e ricontrolliamo
    }
}

/* Sostituisce '\n' con '\0' (utile nel parsing della libreria da file */
void RimuoviNewline(char s[])
{
    #if DEBUG
        printf("\n- %p: '%s'", (void *)s, s);
    #endif // DEBUG

    int i=0;
    while(s[i] != '\n') // Cerco il primo '\n'
    {
        #if DEBUG
            printf("%d ", i);
        #endif // DEBUG
        i++;
    }
    s[i]='\0'; // Sostituisco "tagliando" la stringa

    #if DEBUG
        printf(" [%d] -> '%s'\n\n", i, s);
    #endif // DEBUG

}

/* Funzione che stampa una stringa racchiudendola tra doppi apici */
void PrintfString(char str[]) { printf("\"%s\" ", str); }

/* Funzione che si occupa di mostrare a video le informazini di un libro passato per puntatore */
void MostraLibro(libro *libro)
{
    int i;

    #if DEBUG
        printf("\n%p\n", (void *)libro);
    #endif // DEBUG

    printf("ISBN: ");            PrintfString(libro->ISBN);
    printf("\n->Titolo:   \t");  PrintfString(libro->titolo);
    printf("\n->Autore:  \t");   PrintfString(libro->autore);
    printf("\n->Tags: \t");      for(i=0; i<3; i++) PrintfString(libro->tag[i]);

    #if DEBUG
        printf("\n->NEXT: %p", (void *)libro->next);
    #endif // DEBUG
}

/* Funzione che cancella il primo (ed unico) libro con ISBN... */
void CancellaLibro(libro **IndirizzoLibreria, char *ISBNDaCanc)
{
    #if DEBUG
        printf("CancellaLibro(%p, %s)", (void *)*IndirizzoLibreria, ISBNDaCanc);
    #endif // DEBUG

    libro *prev = NULL,
          *elem = *IndirizzoLibreria;

    while(elem) // Cerchiamo il libro
    {
        if(!strcmp(elem->ISBN, ISBNDaCanc))
        {
            /* Se il libro da eliminare � il primo, spostiamo l'indirizzo di "libreria" al secondo elemento della lista */
            if(elem == *IndirizzoLibreria) *IndirizzoLibreria = (*IndirizzoLibreria)->next;
            else prev->next = elem->next;

            #if DEBUG
                printf("Libro in %p cancellato!", (void *)elem);
            #endif // DEBUG

            /* Liberiamo la memoria occupata prima dal libro lasciato fuori dalla libreria */
            free((void *)elem);
            return;
        }
        /* Avanziamo */
        prev = elem;
        elem = elem->next;
    }

    if(!elem) printf("ISBN non trovato!");
}

/* Mostra libreria */
void MostraLibreria(libro **IndirizzoLibreria)
{
    #if DEBUG
        printf("MostraLibreria(%p)", (void *)*IndirizzoLibreria);
    #endif // DEBUG

    printf("\n\nLibreria:\n");

    libro *elem = *IndirizzoLibreria;
    int i=0;

    while(elem)
    {
        printf("\n\nLibro %d:\n", i+1);
        MostraLibro(elem);

        elem = elem->next;
        i++;
    }

    printf("\n");
}



void MainMenu(libro **IndirizzoLibreria)
{
    #if DEBUG
        printf("MainMenu()");
    #endif // DEBUG

    while(1)
    {
        printf("\n\nChe operazione desidera effettuare?\n");
        printf("0. Mostra Libreria\n");
        printf("1. Inserimento di libri\n");
        printf("2. Cancellazione di libri\n");
        printf("3. Ricerca di libri per tags\n");
        printf("4. Salva ed esci\n");
        printf("\n(0/1/2/3/4) -> ");

        int scelta_menu;
        scanf("%d", &scelta_menu);
        fgets(&junk, 2, stdin); /// Consumiamo un carattere dallo stdin

        char SceltaSeContinuare; // Var. usata per le opzioni 1 2 e 3
        switch(scelta_menu)
        {
            case 0: MostraLibreria(IndirizzoLibreria);
            break;
            case 1: /// Inserimento
                /* Con un ciclo while, carichiamo quanti libri l'utente vuole inserire (nel limite dello spazio disponibile) */
                SceltaSeContinuare='S';
                while(SceltaSeContinuare!='N' && SceltaSeContinuare!='n')
                {
                    /* Se fallisce l'inserimento (causa memoria) torniamo al menu principale */
                    if(!Inserisci(IndirizzoLibreria)) break;

                    /* Altrimenti chiediamo se se ne vuole inserire un altro */
                    printf("\n\n");
                    printf("Vuole inserire un altro libro? (S/N)\n");
                    scanf("%c", &SceltaSeContinuare);
                    fgets(&junk, 2, stdin); /// Consumiamo un carattere dallo stdin
                }
            break;
            case 2: /// Cancellazione
                /* Con un ciclo while, cancelliamo quanti libri l'utente vuole cancellare */
                SceltaSeContinuare='S';
                while(SceltaSeContinuare!='N' && SceltaSeContinuare!='n')
                {
                    /* Chiamiamo la funzione di cancellazione */
                    Cancellazione(IndirizzoLibreria);

                    printf("\n\n");
                    printf("Vuole cancellare un altro libro? (S/N)\n");
                    scanf("%c", &SceltaSeContinuare);
                    fgets(&junk, 2, stdin); /// Consumiamo un carattere dallo stdin
                }
            break;
            case 3: /// Ricerca
                /* Con un ciclo while, cerchiamo libri quante volte l'utente vuole */
                SceltaSeContinuare='S';
                while(SceltaSeContinuare!='N' && SceltaSeContinuare!='n')
                {
                    /* Chiamo la funzione di ricerca */
                    Ricerca(*IndirizzoLibreria);

                    printf("\n\n");
                    printf("Vuole cercare altri libri? (S/N)\n");
                    scanf("%c", &SceltaSeContinuare);
                    fgets(&junk, 2, stdin); /// Consumiamo un carattere dallo stdin
                }
            break;
            case 4: /// Termine del menu (il flusso torna in main per salvare la libreria)
                return;
            break;
            default:
                printf("Comando non valido\n");
        }
    }
}

/* Funzione di cancellazione */
int Cancellazione(libro **IndirizzoLibreria)
{
    #if DEBUG
        printf("Cancellazione(%p)", (void *)*IndirizzoLibreria);
    #endif // DEBUG

    printf("\n\nCancellazione:\n");

    /* Ricerchiamo e mostriamo i libri con i tags digitati */
    Ricerca(*IndirizzoLibreria);
    printf("\n");

    char ISBNLibroDaCanc[20];

    do {
        printf("\nDigitare l'ISBN del libro da cancellare: ");
        if(fgets(ISBNLibroDaCanc, 20, stdin) != NULL) break;
        else printf("Errore nella lettura di \"stdin\".\n");
    } while(1);

    PulisciStringa(ISBNLibroDaCanc);

    /* Cerchiamo il primo (e unico) libro con ISBN uguale e lo cancelliamo dalla lista */
    CancellaLibro(IndirizzoLibreria, ISBNLibroDaCanc);

    return 0;
}

/* Funzione di inserimento */
int Inserisci(libro **IndirizzoLibreria)
{
    /* Puntatore di controllo dedicato all'inserimento */
    libro *index;

    #if DEBUG
        printf("Inserisci in (%p)", (void *)*IndirizzoLibreria);
    #endif // DEBUG

    printf("\n\nInserimento:\n");

    /* Se la lista � vuota allochiamo nuovo spazio facendolo puntare direttamente dalla testa */
    if(*IndirizzoLibreria == NULL)
    {
        *IndirizzoLibreria = (libro *) malloc(sizeof(libro));
        index=*IndirizzoLibreria;
    }
    else /* Altrimenti allochiamo un nuovo next all'ultimo elemento della lista */
    {
        index=*IndirizzoLibreria;
        while(index->next) // cerchiamo l'ultimo elemento
        {
            #if DEBUG
                printf("(%p) (%p)->", (void *)index, (void *)index->next);
            #endif // DEBUG
            index = index->next;
        }

        /* Allochiamo memoria in next e avanziamo index preparandolo per l'inserimento */
        index->next = (libro *) malloc(sizeof(libro));
        index = index->next;
    }

    #if DEBUG
        printf("Alloco in (%p)", (void *)index);
    #endif // DEBUG

    if(!index) // Piccolo controllo per evitare crash inaspettati in condizioni estreme
    {
        printf("\n\nATTENZIONE! MEMORIA ESAURITA!\n\n");
        return 0;
    }

    #if DEBUG
        printf("Allocando in (%p)", (void *)index);
    #endif // DEBUG

    /* "Tappiamo" subito la lista */
    index->next=NULL;

    /* Inserimento controllato dell'ISBN */
    do {
        /* Otteniamo un ISBN */
        do {
            printf("- ISBN: ");

            if(fgets(index->ISBN, 20, stdin) != NULL) break;
            else printf("Errore nella lettura di \"stdin\".\n");
        } while(1);

        PulisciStringa(index->ISBN);

        /* E controlliamo che non esista gi� */
        int EsisteGia = 0;
        libro *elem = *IndirizzoLibreria;

        while(elem)
        {
            if(!strcmp(elem->ISBN, index->ISBN) && elem!=index) EsisteGia = 1;
            elem = elem->next;
        }

        if(!EsisteGia) break;
        else printf("GIA' PRESENTE!\n");
    } while(1);

    /* Inserimento Titolo */
    do {
        printf("- Titolo: ");

        if(fgets(index->titolo, 50, stdin) != NULL) break;
        else printf("Errore nella lettura di \"stdin\".\n");
    } while(1);

    PulisciStringa(index->titolo);

    /* Inserimento Autore */
    do {
        printf("- Autore: ");

        if(fgets(index->autore, 30, stdin) != NULL) break;
        else printf("Errore nella lettura di \"stdin\".\n");
    } while(1);

    PulisciStringa(index->autore);

    /* Inserimento controllato Tags */
    do {
        printf("- 3 Tags (separati dagli spazi, senza apici o doppi apici): ");

        /* Memorizziamo i tags come una stringa di 3 parole spaziate tra loro */
        char StringaDiTags[90];
        if(fgets(StringaDiTags, 90, stdin) == NULL)
        {
            printf("Errore nella lettura di \"stdin\".\n");
            continue;
        }

        PulisciStringa(StringaDiTags);

        /* Ricaviamo le keywords dall'input dell'utente */
        int i=0;
        char *token = NULL;
        token = strtok(StringaDiTags, " \n");

        while(token != NULL)
        {
            strcpy(index->tag[i], token);
            token = strtok(NULL, " ");
            i++;
        }

        /* Se non sono state inserite esattamente 3 tags le richiediamo */
        if(i==3) break;
        else
        {
            printf("Errore! Per favore inserire esattamente 3 tags\n");
            continue;
        }
    } while(1);

    /* Mostriamo il libro */
    MostraLibro(index);
    return 1;
}

/* Funzione di ricerca */
void Ricerca(libro *libreria)
{
    #if DEBUG
        printf("Ricerca(%p)", (void *)libreria);
    #endif // DEBUG

    printf("\n\nRicerca:\n");

    /* Richiediamo all'utente le 3 parole chiave da cercare */

    char TagDaCercare[3][30];
    int i;
    do {
        printf("Inserire 3 Tags (separati dagli spazi, senza apici o doppi apici): ");

        char StringaDiTags[90];

        if(fgets(StringaDiTags, 90, stdin) == NULL)
        {
            printf("Errore nella lettura di \"stdin\".\n");
            continue;
        }

        PulisciStringa(StringaDiTags);

        /* Ricaviamo le keywords dall'input dell'utente */
        int i=0;
        char *token = NULL;
        token = strtok(StringaDiTags, " \n");

        while(token != NULL)
        {
            strcpy(TagDaCercare[i], token);
            token = strtok(NULL, " ");
            i++;
        }

        /* Se non sono state inserite esattamente 3 tags le richiediamo */
        if(i==3) break;
        else
        {
            printf("Errore! Per favore inserire esattamente 3 tags\n");
            continue;
        }
    } while(1);

    /* Ricerca "ottimizzata": mostriamo i libri che contengono prima tutte e 3 le parole chiave,
        poi quelli che ne contengono 2, e poi solo una */
    for(i=3; i>0; i--)
    {
        /// printf("\n___________%d Tags Comuni:___________\n", i);
        MostraLibriConTag(libreria, (char **)TagDaCercare, i);
    }

}

/* Funzione-satellite di Ricerca() */
void MostraLibriConTag(libro *libreria, char **KWTS, int NumTagComuniDaCarcare)
{
    /* Recupero le 2 dimensioni dell'array TagDaCercare, perse nella chiamata */
    char (*TagDaCercare)[30] = (char (*)[30])KWTS;

    #if DEBUG
        printf("Ricerca(%p, %p, %d)", (void *)libreria, (void *)&KWTS[0][0], NumTagComuniDaCarcare);
    #endif // DEBUG

    libro *ptr = libreria;
    while(ptr) /* Per ogni libro */
    {
        int num_KWToRicerca;

        int contatore=0;

        /* confrontiamo ognuno dei tag con ognuno di quelli da cercare */
        for(num_KWToRicerca=0; num_KWToRicerca<3; num_KWToRicerca++)
        {
            int num_KWlibro;
            for(num_KWlibro=0; num_KWlibro<3; num_KWlibro++)
            {
                /* Ogni volta che troviamo una corrispondenza */
                if(strcmp(TagDaCercare[num_KWToRicerca], ptr->tag[num_KWlibro]) == 0)
                {
                    /* Aumentiamo un contatore */
                    contatore++;
                }
            }
        }
        // e mostriamo il libro solamente se ha il numero esatto NumTagComuniDaCarcare di tag in comune con quelli digitati
        if(contatore == NumTagComuniDaCarcare) MostraLibro(ptr);

        ptr = ptr->next;
    }
}

/* Funzione che inizializza la libreria a zero o da file, se presente */
void CaricaLibreria(libro **IndirizzoLibreria)
{
    #if DEBUG
        printf("CaricaLibreria in %p", (void *)*IndirizzoLibreria);
    #endif // DEBUG

    /* Apriamo il file */
	FILE *LIBRERIA = fopen("library.khe", "rb");

    int NuovaLibreria = 0;

	// Se esiste
	if(LIBRERIA)
    {
        /* Inizializzo la libreria come un puntatore ad un libro dinamicamente allocato */
        libro *new = (libro *) malloc(sizeof(libro));
        libro *prev = NULL; /* Variabile per tenere traccia dell'elemento precedente a new,
                                in modo da "tappare" correttamente la lista */

        #if DEBUG
            printf("Carico libri...");
        #endif // DEBUG

        int NumLibri = 0, error = 0; // error diventa vera se il file libreria � corrotto

        while(fgets(new->ISBN, 20, LIBRERIA)) /* Per ogni nuovo ISBN letto e copiato direttamente in coda */
        {
            #if DEBUG
                printf("\nLibro %d: ", NumLibri+1);
            #endif // DEBUG

            /* Si copiano anche gli altri valori, verificando che non accadano errori */
            if(!fgets(new->autore, 30, LIBRERIA)) error = 1;
            if(!fgets(new->titolo, 50, LIBRERIA)) error = 1;
            if(!fgets(new->tag[0], 30, LIBRERIA)) error = 1;
            if(!fgets(new->tag[1], 30, LIBRERIA)) error = 1;
            if(!fgets(new->tag[2], 30, LIBRERIA)) error = 1;

            /* Se il file libreria � corrotto usciamo dal ciclo direttamente (l'ultimo libro, "salvato a met�", viene ignorato */
            if(error) break;
            else /* Altrimenti */
            {
                /* Puliamo le stringhe ottenute dal file */
                RimuoviNewline(new->ISBN);
                RimuoviNewline(new->autore);
                RimuoviNewline(new->titolo);
                RimuoviNewline(new->tag[0]);
                RimuoviNewline(new->tag[1]);
                RimuoviNewline(new->tag[2]);

                #if DEBUG
                    MostraLibro(new);
                #endif // DEBUG

                /* Appendiamo l'elemento alla lista
                    (se � il primo, copiamo il puntatore in testa, altrimenti lo copiamo nel next dell'ultimo elemento) */
                if(!NumLibri) *IndirizzoLibreria = new;
                else prev->next = new;

                /* Prepariamo la lista per il prossimo elemento:
                    - teniamo l'indirizzo di questo elemento in prev per collegare poi un eventuale elemento alla prossima iterazione
                    - tappiamo preventivamente la lista nel caso l'elemento appena memorizzato sia l'ultimo
                    - allochiamo nuova memoria puntata da new per il prossimo eventuale libro */
                prev = new;
                new->next = NULL;
                new = (libro *) malloc(sizeof(libro));
            }

            NumLibri++;
        }

        /* Se vi sono stati errori new punta ad un libro non completamente allocato, che verr� ignorato
            se non ve ne sono stati, new contiene un puntatore ad un libro non ancora caricato.
            In entrambi i casi non contiene informazioni utili: liberiamo la memoria a cui punta */
        free((void *)new);

        if(NumLibri)
        {
            printf("Benvenuto nella sua libreria: %d libri caricati", NumLibri);
            if(error) printf(" (1 errore nella lettura dell'ultimo libro)");
            printf("\n");
        }
        else // Se nessun libro � stato caricato completamente avvertiamo
        {
            NuovaLibreria = 1;
            printf("Nessun libro caricato!");
        }

		// Tentativo di chiusura del file
		if (fclose(LIBRERIA) == EOF)
        {
			printf("Errore nella chiusura della libreria esistente\n");
			exit(1);
		}
	}
	else // Se non vi � nessun file...
	{
        /* Inizializziamo la libreria come puntatore a memoria non valida */
        *IndirizzoLibreria = NULL;
        NuovaLibreria = 1;
	}

	if(NuovaLibreria) printf("Benvenuto nella sua nuova libreria!\n");
}

/* Funzione di salvataggio su file della libreria */
void SalvaLibreria(libro **IndirizzoLibreria)
{
	// Apro il file in scrittura (eventualmete sovrascrivendo
	FILE *LIBRERIA = fopen("library.khe", "wb");

	if(LIBRERIA)
    {
        libro *LibroDaSalvare = *IndirizzoLibreria;

        #if DEBUG
            printf("SalvaLibreria(%p)", (void *)*IndirizzoLibreria);
        #endif // DEBUG

        /* Salvo ogni libro nel formato "ISBN\nAUTORE\nTITOLO\nTAG1\nTAG2\nTAG3\nISBN2\nAUTORE2\n..." */
        while(LibroDaSalvare)
        {
            fprintf(LIBRERIA, "%s\n%s\n%s\n%s\n%s\n%s\n",
                    LibroDaSalvare->ISBN,
                    LibroDaSalvare->autore,
                    LibroDaSalvare->titolo,
                    LibroDaSalvare->tag[0],
                    LibroDaSalvare->tag[1],
                    LibroDaSalvare->tag[2]
                    );

            LibroDaSalvare = LibroDaSalvare->next;
        }

        // Tentativo di chiusura del file
		if (fclose(LIBRERIA) == EOF)
        {
			printf("Errore in chiusura del salvataggio della libreria\n");
			exit(1);
		}
	}
	else
	{
        printf("Errore nel salvataggio della libreria\n");
        exit(1);
	}
}
