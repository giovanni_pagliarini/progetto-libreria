#ifndef _LIBRO

#define _LIBRO

/* Variabili utili... */
char junk;

/* Struttura che contiene le informazioni riguardanti un libro della libreria */
struct _libro
{
    char ISBN[20];
    char autore[30];
    char titolo[50];
    char tag[3][30];
    struct _libro *next;
};
typedef struct _libro libro;

/* Prototipi delle funzioni che user� main */
void Cerca(libro *);
void MostraLibriConTag(libro *, char **, int);

void CaricaLibreria(libro **);
void MostraLibreria(libro **);
void MainMenu(libro **);
void SalvaLibreria(libro **);

int Inserisci(libro **);
int Cancellazione(libro **);
void Ricerca(libro *);

#endif // LIBRO
