#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/// Per vedere a video il flusso del programma impostare a 1
#define DEBUG 0

#include "libro.h"

int main()
{
    /* Inizializziamo la libreria come puntatore a libro non valido  */
    libro *libreria = NULL;

	#if DEBUG
        printf("Puntatore testa memorizzato in %p\n", (void *)&libreria);
	#endif // DEBUG

    /* Carichiamo la libreria (inizializzandola da zero o da un eventuale file) */
	CaricaLibreria(&libreria);

	/* Chiamiamo la funzione di controllo del menu, la quale fornisce l'interfaccia all'utente */
    MainMenu(&libreria);

	/* Salviamo la libreria modificata su file */
    SalvaLibreria(&libreria);

    return 0;
}
