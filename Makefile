all: libreria

libro.o: libro.c
	gcc -Wall -c libro.c

libreria.o: libreria.c
	gcc -Wall -c libreria.c

libreria: libreria.o libro.o
	gcc -Wall -o libreria libreria.o libro.o

clean: 
		rm *.o
		

